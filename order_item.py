from immutable import Immutable


class OrderItem(Immutable):
    __slots__ = ('name', 'itemnumber', 'quantity','price', 'backordered')

    name = ''
    itemnumber = ''
    quantity = 0
    price = 0
    backordered = False

    def __init__(self, name, itemnumber, quantity, price, backordered) -> None:
        super().__init__()
        self.name = name
        self.itemnumber = itemnumber
        self.quantity = quantity
        self.price = price
        self.backordered = backordered

    @property
    def total_price(self):
        return self.quantity * self.price
