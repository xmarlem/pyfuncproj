# Orders project (Functional programming in Python)


## 3 First class functions

### Premessa

- Order: e' una class che contiene la lista degli ordini, e tre metodi per:
  - ottenere la lista dei nomi di customer di tutti gli ordini "expedited" (accelerati) (attributo dell'oggetto customer)
  - ottenere la lista degli indirizzi di spedizione di tutti gli orgini accelerati (attributo dell'oggetto order)
  - ottenere la lista degli indirizzi dei customer di tutti gli ordini accelerati

### Refactoring 1 - Redundancy

in Order ci sono troppe volte ripetute sempre le stess linee di codice... in particolare, si scorre attraverso la lista degli ordini troppe volte.

24 lines of duplicated code

#### Step 1

notiamo che i tre metodi non usano l'oggetto self. Quindi potrebbero essere statiche! Quindi "pure functions".
Trasformo i tre metodi in metodi statici
#### Step 2
Mi rendo conto che devo introdurre funzioni pure a supporto delle altre...
La strategia e' passare un oggetto order in input e restituire un risultato (sempre lo stesso)


- introduco una funzione test_expedited e rimpiazzo il test expedited in tutte le funzioni

```python
    @staticmethod
    def test_expedited(order):
        return order.expedited
```

#### Step 3

Ora mi serve una high order function (funzione che restituisce o a cui viene passata una funzione)

- genero una nuova funzione di supporto get_filtered_orders_customer_names che prende un predicato come input
  - questo predicato sara' la nostra funzione test... gliela passiamo dall'esterno, in tal modo la funzione diventa piu' generica

- aggiungiamo anche un altra funzione pura, che restituisce il customer name dato un ordine

```python
    @staticmethod
    def get_filtered_orders_customer_names(predicate):
        output = []
        for order in Order.orders:
            if predicate(order):
                output.append(Order.get_customer_name(order))
        return output

    @staticmethod
    def get_customer_name(order):
        return order.customer.name
```
#### Step 4

Mi rendo conto che la funzione potrebbe essere ancora di piu' generalizzata... le sto passando get customer name, ed anche il nome non e' generico.
Potrei fare di piu'!

Allora:
- introducto una get_filtered_orders(predicate, func)

```python
    @staticmethod
    def get_filtered_orders_customer_names(predicate, func):
        output = []
        for order in Order.orders:
            if predicate(order):
                output.append(func(order))
        return output

    @staticmethod
    def get_customer_name(order):
        return order.customer.name
```

Il metodo ora diventera' cosi:
```python
    @staticmethod
    def get_expedited_orders_customer_names():
        return Order.get_filtered_orders_customer_names(Order.test_expedited, Order.get_customer_name)
```

Inoltre applico la stessa logica anche agli altri metodi... aggiungendo altrettante funzioni pure



### Refactoring 2

#### New functionality: vogliamo poter settare expedited attribute

Ci serve qualcosa per "accelerare" un ordine... something to expedite an order.
Non e' fatto di default.

- aggiungere un metodo set_order_expedited(orderid)

in pratica, scorro tutti gli ordini, se trovo l'ordine corrispondente a quell'id, ne setto l'attributo expedited to true

#### prima implementazione

```python
    @staticmethod
    def set_order_expedited(orderid):
        for o in Order.orders:
            if o.orderid == orderid:
                o.expedited = True
```

ma non sono contento, pensavo di essermi liberato di dover ogni volta iterare....
allora provo ad aggiungere un metogo get_order_by_id

#### aggiunge get_order_by_id

```python

    @staticmethod
    def set_order_expedited(orderid):
        o = Order.get_order_byid(orderid)
        if o:
            o.expedited = True

    @staticmethod
    def get_order_by_id(orderid):
        for o in Order.orders:
            if o.orderid == orderid:
                return o
            else:
                return None
```

#### ma...attenzione!!! Possiamo riusare get_filtered_orders

```python
    @staticmethod
    def set_order_expedited(orderid):
        o = Order.get_order_byid(orderid)
        if o:
            o.expedited = True

    @staticmethod
    def get_order_by_id(orderid):
        return Order.get_filtered_orders(
            lambda o : o.orderid == orderid,
            lambda o : o,
        )
```

NB. ora, implicitamente gestisce anche il caso di piu' ordini con lo stesso id

Inoltre, se osservo bene, posso anche eliminare quel controllo se c'e' un ordine... anzi, ora mi genererebbe un errore perche' devo gestire una lista, non piu' un solo elemento.


Ecco la versione finale:

``` python

    @staticmethod
    def set_order_expedited(orderid):
        for o in Order.get_order_byid(orderid):
            o.expedited = True

    @staticmethod
    def get_order_by_id(orderid):
        return Order.get_filtered_orders(
            lambda o : o.orderid == orderid,
            lambda o : o,
        )

```

#### Ora dobbiamo rendere le nostre funzioni la piu' pura possibile

Ci rendiamo ad esempio conto che get_filtered_orders ha due responsabilita':
- filtraggio
- selezione di un attributo nell'oggetto order


Come fare?

Estraiamo due funzioni... pero' sfruttando gia' built-in functions che abbiamo...
ne facciamo solo un adattamento... in quanto, filter restituisce un iterator
stessa cosa per map


Ecco come diventa:

*Before*
```python
    @staticmethod
    def get_filtered_info(predicate, func):
        output = []
        for order in Order.orders:
            if predicate(order):
                output.append(func(order))
        return output
```

*After*

```python

    @staticmethod
    def get_filtered_info(predicate, func, orders):
        return Order.map(func, Order.filter(predicate, orders))

    @staticmethod
    def filter(predicate, it):
        return list(filter(predicate, it))

    @staticmethod
    def map(func, it):
        return list(map(func, it))

```

Ma posso migliorare anche gli altri metodi, solo con questi due metodi

*Prima*:

```python
    @staticmethod
    def get_order_by_id(orderid):
        return Order.get_filtered_info(
            lambda o : o.orderid == orderid,
            lambda o : o,
        )
```

*Dopo*:

```python
    @staticmethod
    def get_order_by_id(orderid, orders):
        return Order.filter(lambda o : o.orderid == orderid, orders)

```
### Refactoring 3 - Immutable variables

Non e' cosa buona cambiare variabili globali dall'interno di funzioni....
es. con la parola chiave:

global nomevariable

Potrei rompere il codice di altri...
Ci sono molti vantaggi nelle variabili immutable.
#### Backordered notification

Un ordine ha N items associati.
Nel caso in cui uno degli items dovesse avere un ritardo.. allora dovremmo notificare il customer.

Inoltre, dovremmo avere una funzionalita' che marchi un ordine come "backordered"


#### Step 1

- aggiunto __init__ alla class order_item
- aggiunto reference nella order class

- aggiungere notify_backordered(orders, msg)

    se dovessi tornare a iterare etc... non sarebbe programmazione funzionale...
    devo cambiare strategia

Ecco l'implementazione della notify...

```python
    @staticmethod
    def notify_backordered(orders, msg):
        Order.map(
            lambda o : o.customer.notify(o.customer.msg),
            Order.filter(
                lambda oi : oi.backordered == True,
                orders.order_items
            )
        )
```

#### Step 2

Ora mi serve qualcosa per marcare un order item come backordered

TODO: find an order item and set it to backordered
--> ma questo significherebbe che sto mutando quella variabile e l'oggetto stesso, ed e' quello che non voglio fare.

Dobbiamo farlo in un modo "immutable".
Magari lo possiamo fare facendo in modo che "immutability" sia il default state di un object. Bloccare tutto e fare il raise di un exception nel caso qualcuno voglia modificarlo.

Come faccio? Creao una classe base.

##### Caratteristiche
- requirements:
  - mi serve qualcosa per bloccare la possibilita' di creare attributi a runtime --> \__slots__
  - mi serve qualcosa per far si che un valore sia settato solo la prima volta agli attributi --> \__setattr__
  - devo impedire che qualcuno cancelli un attributo... --> \__delattr__


- uso ABCMeta come metaclass, cosi che ho il check a tempo di sviluppo e non a runtime...
- uso \__slots__:  in tal modo, a parte risparmiare memoria, vado a bloccare la possibilita' di aggiungere altri attributi.
- uso \__setattr__  per assegnare solo la prima volta
  - nb. per incrementare \__attrs__ con altri valori man mano...essendo un frozenset devo usare l'union e ricreare ogni volta l'oggetto da referenziare
- uso \__delattr__ per impedire la rimozione di attributi


Ho creato la classe immutable
E ho rinforzato tutte le classi ereditando da questa class. Sono quindi divenute tutte immutable.
#### Step 3

Ora dobbiamo tornare al nostro problema. Dobbiamo implementare il metodo che ci permetta di fare il mark di un order item come backordered.
Ma, ricordiamo, visto che abbiamo messo l'enforcemente, non possiamo piu' cambiare l'attributo su order item.
Come facciamo?
Dobbiamo ritornare un nuovo oggetto.
Non solo, dobbiamo modificare la lista degli ordini... ma non possiamo, questo significa restituire una nuova lista rimpiazzando l'ordine modificato!!


Eccolo qui implementato (metodo):

```python
    @staticmethod
    def mark_backordered(orders, orderid, itemnumber):
        return Order.map(
            # se non e' l'ordine cercato, restituisci lo stesso ordine (o) altrimenti un nuovo ordine con nuovo order_item
            lambda o :
                o if o.orderid != orderid
                else Order(o.orderid, o.shipping_address, o.expedited, o.shipped, o.customer,
                       Order.map(
                           lambda oi:
                               oi if oi.itemnumber == itemnumber
                               else OrderItem(oi.name, oi.itemnumber, oi.quantity, oi.price, True),
                           o.order_items
                       )
                ),
                orders
        )
```

Ora sembra che il concetto di fare l'update di un ordine possa essere generalizzato... proviamo a farlo!
(the pattern of updating a collection would be useful in general)

Implementiamo una funzione get_updated_tuple.

```python

def get_updated_tuple(it, predicate, func):
    return tuple(
        func(i) if predicate(i) else i
        for i in it
    )


```


### Refactoring 4 - Lazy evaluation

**Requirement**:
I want to know the total value of all items
I want to have a python property to get this.

A problem can occur is that we have to recalculate this every time.... that's bad!
How to do?
And even worse, we have to recalculate every time the colleciton changes....and maybe this value could be just asked now and then (una volta tanto)

If I would do that in the \__init__ method, that would be a eager evaluation ...
we need lazy evaluation!

Posso:
- definire una property (getter) in order_item: total_price
- definire un total_price anche sull'order che somma i total price su tutti gli items

```python
    @property
    def total_price(self):
        return sum(i.total_price for i in self.order_items)
```

#### Improvement

Si potrebbe usare anche la funzione REDUCE.


#### Problema ... total_price non e' lazy

Ma c'e' uno strumento in python ... lru_cache!!!

In pratica, ogni volta che accedo alla property viene letto da una cache locale...

#### Problema 2 con tuple

Abbiamo un altro problema con get_updated_tuple.... non e' lazy...

OGNI volta che si richiama restituiamo tutta la tuple...
ma che succede se non voglio tutta la tupla, ma solo un valore??
Posso trasformarlo in una generator expression... semplicmeente eliminando tuple.

```python

def get_updated_tuple(it, predicate, func):
    return (
        func(i) if predicate(i) else i
        for i in it
    )

```

in tal modo, si restituisce un generator, che per definition e' lazily evaluated.

Lo rinomino in get_updated_sequence


NB. since python 3.0, molte built-in functions sono lazy.

### GOAL: i want to count expedited orders that have backordered items

(contare il numero di ordini spediti che sono in ritardo)

We want to use tail recursion and tramp function

Tip: se si pensa una list o una tuple come l'insieme del suo primo elemento (head) e il resto (tail)...
