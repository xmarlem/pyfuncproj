import abc

class Immutable(metaclass=abc.ABCMeta):
    __slots__ = ("__attrs__",) # attrs serve per tenere traccia di quali attributi sono stati gia' settati

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.__attrs__ = frozenset() # this way I make sure that

    def __setattr__(self, name: str, value) -> None:
        if name == "__attrs__":
            super().__setattr__(name, value)
            return

        if name not in self.__attrs__:
            # non posso fare cosi... altriementi muterei.... self.__attrs__[name] = value
            super().__setattr__(name, value) # prima setto l'attributo...
            self.__attrs__ |= {name} # nb. col frozenset... posso solo allocare un nuovo oggetto

        else:
            raise AttributeError(f"Attempt to modify immutable attribute: {name}")

    def __delattr__(self, name: str) -> None:
        if name in self.__attrs__:
            raise AttributeError(f"Attempt to delete immutable attribute: {name}")
        else:
            raise AttributeError(name)


if __name__ == '__main__':
    class MyClass(Immutable):
        __slots__ = ("lastname",)

    p = MyClass()

    # no new attributes after creation
    try:
        p.name="Marco"
    except:
        print("Non e' possibile creare nuovi attributi dopo l'instanziazione")

    # only static attribute can be set, but only once
    p.lastname = "Marco"

    try:
        p.lastname ="Michele"
    except AttributeError:
        print("Non e' possibile assegnare un valore piu' di una volta")

    print(p)
