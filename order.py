
from immutable import Immutable
from order_item import OrderItem
from functools import lru_cache, reduce


def get_updated_sequence(it, predicate, func):
    return (
        func(i) if predicate(i) else i
        for i in it
    )

class Order(Immutable):
    __slots__ = ('orderid', 'shipping_address', 'expedited', 'shipped', 'customer', 'order_items',)


    # class attribute
    orders = []

    # instance attributes
    orderid = 0
    shipping_address = ''
    expedited = False
    shipped = False
    customer = None
    order_items = []


    def __init__(self, orderid, shipping_address, expedited, shipped, customer, order_items) -> None:
        self.orderid = orderid
        self.shipping_address = shipping_address
        self.expedited = expedited
        self.shipped = shipped
        self.customer = customer
        self.order_items = order_items

    @staticmethod
    def notify_backordered(orders, msg):
        Order.map(
            lambda o : o.customer.notify(o.customer.msg),
            Order.filter(
                lambda oi : oi.backordered == True,
                orders.order_items
            )
        )

    @staticmethod
    def get_customer_name(order):
        return order.customer.name

    @staticmethod
    def get_customer_address(order):
        return order.customer.address

    @staticmethod
    def get_shipping_address(order):
        return order.shipping_address

    @staticmethod
    def get_expedited_orders_customer_names():
        return Order.get_filtered_info(Order.test_expedited, Order.get_customer_name, Order.orders)

    @staticmethod
    def get_expedited_orders_customer_addresses():
        return Order.get_filtered_info(Order.test_expedited, Order.get_customer_address, Order.orders)

    @staticmethod
    def get_expedited_orders_shipping_addresses():
        return Order.get_filtered_info(Order.test_expedited, Order.get_shipping_address, Order.orders)

    @staticmethod
    def test_expedited(order):
        return order.expedited


    # Questo metodo e' eccezionale! Posso filtrare attraverso il predicato e selezionare l'attributo che mi serve
    @staticmethod
    def get_filtered_info(predicate, func, orders):
        return Order.map(func, Order.filter(predicate, orders))

    @staticmethod
    def set_order_expedited(orderid, orders):
        for o in Order.map(lambda o : o.orderid == orderid, orders):
            o.expedited = True

        # for o in Order.get_order_byid(orderid):
        #     o.expedited = True

    @staticmethod
    def get_order_by_id(orderid, orders):
        return Order.filter(lambda o : o.orderid == orderid, orders)

    @staticmethod
    def filter(predicate, it):
        return list(filter(predicate, it))

    @staticmethod
    def map(func, it):
        return list(map(func, it))

        """Devo restituire una lista di ordini con l'ordine modificato se trovato...

        Returns:
            List: lista ordini "modificata"
        """
    # @staticmethod
    # def mark_backordered(orders, orderid, itemnumber):
    #     return Order.map(
    #         # se non e' l'ordine cercato, restituisci lo stesso ordine (o) altrimenti un nuovo ordine con nuovo order_item
    #         lambda o :
    #             o if o.orderid != orderid
    #             else Order(o.orderid, o.shipping_address, o.expedited, o.shipped, o.customer,
    #                    Order.map(
    #                        lambda oi:
    #                            oi if oi.itemnumber != itemnumber
    #                            else OrderItem(oi.name, oi.itemnumber, oi.quantity, oi.price, True),
    #                        o.order_items
    #                    )
    #             ),
    #             orders
    #     )


    @staticmethod
    def mark_backordered(orders, orderid, itemnumber):
        # restituisce
        return get_updated_sequence(
            orders,
            lambda o : o.orderid == orderid,
            lambda o :
                Order(o.orderid, o.shipping_address, o.expedited, o.shipped, o.customer,
                      get_updated_sequence(
                          o.order_items,
                          lambda oi : oi.itemnumber == itemnumber,
                          lambda oi : OrderItem(oi.name, oi.itemnumber, oi.quantity, oi.price, True)
                      )
                )
        )

    @property
    @lru_cache(maxsize=1)
    def total_price(self):
        return sum(i.total_price for i in self.order_items)

    @staticmethod
    def count_expedited_orders_with_backordered_items_tramp(orders, acc=0):
        """[summary]

            Sample call using tramp function:
                tramp(count_expedited_orders_with_backordered_items_tramp, orders)
        """
        if len(orders) == 0:
            yield acc
        else:
            h = orders[0]
            add = 1 if any(i.backordered for i in h.order_items if h.expedited)
            yield Order.count_expedited_orders_with_backordered_items_tramp(orders[1:], acc + add)
